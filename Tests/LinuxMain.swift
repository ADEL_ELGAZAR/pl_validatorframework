import XCTest

import PL_ValidatorFrameworkTests

var tests = [XCTestCaseEntry]()
tests += PL_ValidatorFrameworkTests.allTests()
XCTMain(tests)
