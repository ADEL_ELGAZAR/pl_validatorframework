import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(PL_ValidatorFrameworkTests.allTests),
    ]
}
#endif
