//
//  ValidatingRules.swift
//  PL_ValidatorFramework
//
//  Created by FAB LAB on 10/15/19.
//  Copyright © 2019 FAB LAB. All rights reserved.
//

import Foundation

/// when need to access use .Required.rule
public enum PL_ValidatingRules {
    case AlphaNumeric(msg:String = "")
    case Alpha(msg:String = "")
    case PaymentCard(acceptedTypes: [PaymentCardType] = [], msg:String = "")
    case CardExpiryMonth(msg:String = "")
    case CardExpiryYear(msg:String = "")
    case CharacterSet(set: CharacterSet, msg:String = "")
    case Confirmation(field: ValidatableField, msg:String = "")
    case Email(msg:String = "")
    case Float(msg:String = "")
    case FullName(msg:String = "")
    case HexColor(msg:String = "")
    case IPV4(msg:String = "")
    case ISBN(msg:String = "")
    case RangeLength(min: Int, max:Int, msg:String = "")
    case ExactLength(length: Int, msg:String = "")
    case MaxLength(length: Int, msg:String = "")
    case MinLength(length: Int, msg:String = "")
    case Password(msg:String = "")
    case PhoneNumber(msg:String = "")
    case Required(msg:String = "")
    case ZipCode(msg:String = "")
    case URL(msg:String = "")
    case Regex(regex: String, msg:String = "")
    case Match(prefix: String, suffix: String, msg:String = "")
    
    public var rule: Rule {
        switch self {
        case .AlphaNumeric(let msg):
            return AlphaNumericRule(message: msg)
            
        case .Alpha(let msg):
            return AlphaRule(message: msg)
            
        case .PaymentCard(let acceptedTypes, let msg):
            return PaymentCardRule(acceptedTypes: acceptedTypes, message: msg)
            
        case .CardExpiryMonth(let msg):
            return CardExpiryMonthRule(message: msg)
            
        case .CardExpiryYear(let msg):
            return CardExpiryYearRule(message: msg)
            
        case .CharacterSet(let set, let msg):
            return CharacterSetRule(characterSet: set, message: msg)
            
        case .Confirmation(let field, let msg):
            return ConfirmationRule(confirmField: field, message: msg)
            
        case .Email(let msg):
            return EmailRule(message: msg)
            
        case .Float(let msg):
            return FloatRule(message: msg)
            
        case .FullName(let msg):
            return FullNameRule(message: msg)
            
        case .HexColor(let msg):
            return HexColorRule(message: msg)
            
        case .IPV4(let msg):
            return IPV4Rule(message: msg)
            
        case .ISBN(let msg):
            return ISBNRule(message: msg)
            
        case .RangeLength(let min, let max, let msg):
            return RangeLengthRule(minLength: min, maxLength: max, message: msg)
            
        case .ExactLength(let length, let msg):
            return ExactLengthRule(length: length, message: msg)
            
        case .MaxLength(let length, let msg):
            return MaxLengthRule(length: length, message: msg)
            
        case .MinLength(let length, let msg):
            return MinLengthRule(length: length, message: msg)
            
        case .Password(let msg):
            return PasswordRule(message: msg)
            
        case .PhoneNumber(let msg):
            return PhoneNumberRule(message: msg)
            
        case .Required(let msg):
            return RequiredRule(message: msg)
            
        case .ZipCode(let msg):
            return ZipCodeRule(message: msg)
            
        case .URL(let msg):
            return URLRule(message: msg)
            
        case .Regex(let regex, let msg):
            return RegexRule(regex: regex, message: msg)
            
        case .Match(let prefix, let suffix, let msg):
            return MatchRule(prefix: prefix, suffix: suffix, message: msg)
        }
    }
}
