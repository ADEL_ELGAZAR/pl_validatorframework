//
//  StringCase.swift
//  PL-ValidatorFramework
//
//  Created by FAB LAB on 11/16/19.
//

import Foundation

public enum StringCase:String {
    case UpperCase = "^.*?[A-Z].*?$"
    case LowerCase = "^.*?[a-z].*?$"
    case NumberCase = ".*\\d.*" // Need to be tested
}
