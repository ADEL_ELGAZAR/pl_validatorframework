//
//  Localization.swift
//  PL_ValidatorFramework
//
//  Created by FAB LAB on 10/21/19.
//  Copyright © 2019 FAB LAB. All rights reserved.
//

import Foundation

extension Bundle {
    
    private static let bundleID = "PL-ValidatorFramework"
    
    static var module: Bundle {
        return Bundle(identifier: bundleID) ?? .main
    }
    
}

extension String {
    
    public func localized(withComment comment: String = "") -> String {
        return NSLocalizedString(self, bundle: Bundle.module, comment: comment)
    }
    
}

public enum Localization {
    
    // keys
    public static let AlphaNumeric = "AlphaNumeric".localized()
    public static let Alpha = "Alpha".localized()
    public static let PaymentCard = "PaymentCard".localized()
    public static let CardExpiryMonth = "CardExpiryMonth".localized()
    public static let CardExpiryYear = "CardExpiryYear".localized()
    public static let CharacterSet = "CharacterSet".localized()
    public static let Confirmation = "Confirmation".localized()
    public static let Email = "Email".localized()
    public static let Float = "Float".localized()
    public static let FullName = "FullName".localized()
    public static let HexColor = "HexColor".localized()
    public static let IPV4 = "IPV4".localized()
    public static let ISBN = "ISBN".localized()
    public static let RangeLength = "RangeLength".localized()
    public static let ExactLength = "ExactLength".localized()
    public static let MaxLength = "MaxLength".localized()
    public static let MinLength = "MinLength".localized()
    public static let Password = "Password".localized()
    public static let PhoneNumber = "PhoneNumber".localized()
    public static let Regex = "Regex".localized()
    public static let Required = "Required".localized()
    public static let ZipCode = "ZipCode".localized()
    public static let URL = "URL".localized()
    
}
