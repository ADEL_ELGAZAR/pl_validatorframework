//
//  MaxLengthRule.swift
//  Validator
//
//  Created by Guilherme Berger on 4/6/15.
//

import Foundation

/**
 `MaxLengthRule` is a subclass of `ExactLengthRule` that defines how maximum character length is validated.
 */
class MaxLengthRule: ExactLengthRule {
    
    /**
     Initializes a `MaxLengthRule` object that is to validate the length of the text of a field.
     
     - parameter length: Maximum character length.
     - parameter message: String of error message.
     - returns: An initialized object, or nil if an object could not be created for some reason that would not result in an exception.
     */
    public override init(length: Int, message : String){
        let msg = message.isEmpty ? Localization.MaxLength : message
        super.init(length: length, message: msg)
    }
    
    /**
     Used to validate a field.
     
     - parameter value: String to checked for validation.
     - returns: A boolean value. True if validation is successful; False if validation fails.
     */
    public override func validate(_ value: String) -> Bool {
        return value.count <= length
    }
}
