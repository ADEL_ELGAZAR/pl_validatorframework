//
//  Required.swift
//  pyur-ios
//
//  Created by Jeff Potter on 12/22/14.
//  Copyright (c) 2015 jpotts18. All rights reserved.
//

import Foundation

/**
 `MatchRule` is a subclass of Rule that defines how a required field is validated.
 */
class MatchRule: Rule {
    /// String that start with
    private var prefix : String
    /// String that end with
    private var suffix : String
    /// String that holds error message.
    private var message : String
    
    /**
     Initializes `MatchRule` object with error message. Used to validate a field that matches prefix and suffix.
     
     - parameter prefix: String of text that start with.
     - parameter suffix: String of text that end with.
     - parameter message: String of error message.
     - returns: An initialized `MatchRule` object, or nil if an object could not be created for some reason that would not result in an exception.
     */
    public init(prefix:String, suffix:String, message : String){
        self.prefix = prefix
        self.suffix = suffix
        self.message = message.isEmpty ? Localization.Required : message
    }
    
    /**
     Validates a field.
     
     - parameter value: String to check for validation.
     - returns: Boolean value. True if validation is successful; False if validation fails.
     */
    open func validate(_ value: String) -> Bool {
        return value.hasPrefix(prefix) && value.hasSuffix(suffix)
    }
    
    /**
     Used to display error message when validation fails.
     
     - returns: String of error message.
     */
    open func errorMessage() -> String {
        return message
    }
}
