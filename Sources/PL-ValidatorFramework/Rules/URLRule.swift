import Foundation

/**
 `URLRule` is a subclass of `Rule` that defines how an url is validated.
 */
class URLRule: Rule {
    /// Default error message to be displayed if validation fails.
    private var message : String = Localization.URL
    
    /**
     Initializes an `URLRule` object to validate an url field.
     
     - parameter message: String of error message.
     - returns: An initialized object, or nil if an object could not be created for some reason that would not result in an exception.
     */
    public init(message : String){
        self.message = message.isEmpty ? Localization.URL : message
    }
    
    /**
     Used to validate a field.
     
     - parameter value: String to checked for validation.
     - returns: A boolean value. True if validation is successful; False if validation fails.
     */
    public func validate(_ value: String) -> Bool {
        if value.isEmpty {
            return false
        }
        return NSURL(string: value) != nil
    }

    /**
     Displays an error message if a field fails validation.
     
     - returns: String of error message.
     */
    public func errorMessage() -> String {
        return message
    }
}
