//
//  ExactLengthRule.swift
//  Validator
//
//  Created by Jeff Potter on 2/3/16.
//  Copyright © 2016 jpotts18. All rights reserved.
//

import Foundation

/**
 `RangeLengthRule` is a subclass of Rule that is used to make sure a the text of a field is an exact length.
 */
class RangeLengthRule : Rule {
    /// parameter message: String of error message.
    public var message : String = Localization.RangeLength
    /// Default maximum character length.
    private var MAX_LENGTH: Int = Int.max
    /// Default minimum character length.
    private var MIN_LENGTH: Int = 0
    
    /**
     Initializes an `RangeLengthRule` object to validate the text of a field against an exact length.
     
     - parameter length: Integer value of exact string length being specified.
     - parameter message: String of error message.
     - returns: An initialized `ExactLengthRule` object, or nil if an object could not be created for some reason. that would not result in an exception.
     */
    public init(minLength: Int = 0, maxLength: Int = Int.max, message: String){
        self.MIN_LENGTH = minLength
        self.MAX_LENGTH = maxLength
        let msg = message.isEmpty ? Localization.RangeLength : message
        self.message = String(format: msg, self.MIN_LENGTH, self.MAX_LENGTH)
    }
    
    /**
     Used to validate a field.
     
     - parameter value: String to checked for validation.
     - returns: A boolean value. True if validation is successful; False if validation fails.
     */
    public func validate(_ value: String) -> Bool {
        return value.count >= MIN_LENGTH && value.count <= MAX_LENGTH
    }
    
    /**
     Displays error message if a field fails validation.
     
     - returns: String of error message.
     */
    public func errorMessage() -> String {
        return message
    }
}
