//
//  LengthRule.swift
//  Validator
//
//  Created by Jeff Potter on 3/6/15.
//  Copyright (c) 2015 jpotts18. All rights reserved.
//

import Foundation

/**
 `MinLengthRule` is a subclass of `ExactLengthRule` that defines how minimum character length is validated.
 */
class MinLengthRule: ExactLengthRule {
    /**
     Initializes a `MinLengthRule` object that is to validate the length of the text of a field.
     
     - parameter length: Minimum character length.
     - parameter message: String of error message.
     - returns: An initialized `MinLengthRule` object, or nil if an object could not be created for some reason that would not result in an exception.
     */
    public override init(length: Int, message : String){
        let msg = message.isEmpty ? Localization.MinLength : message
        super.init(length: length, message: msg)
    }
    
    /**
     Validates a field.
     - parameter value: String to checked for validation.
     - returns: A boolean value. True if validation is successful; False if validation fails.
     */
    public override func validate(_ value: String) -> Bool {
        return value.count >= length
    }
}
