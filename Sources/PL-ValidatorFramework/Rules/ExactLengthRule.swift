//
//  ExactLengthRule.swift
//  Validator
//
//  Created by Jeff Potter on 2/3/16.
//  Copyright © 2016 jpotts18. All rights reserved.
//

import Foundation

/**
 `ExactLengthRule` is a subclass of `ExactLengthRule` that is used to make sure a the text of a field is an exact length.
 */
class ExactLengthRule : RangeLengthRule {
    /// parameter length: Integer value string length
    public var length : Int
    
    /**
     Initializes an `ExactLengthRule` object to validate the text of a field against an exact length.

     - parameter length: Integer value of exact string length being specified.
     - parameter message: String of error message.
     - returns: An initialized `ExactLengthRule` object, or nil if an object could not be created for some reason. that would not result in an exception.
     */
    public init(length: Int, message: String){
        self.length = length
        let msg = message.isEmpty ? Localization.ExactLength : message
        super.init(message: msg)
        self.message = String(format: msg, self.length)
    }
    
    /**
     Used to validate a field.
     
     - parameter value: String to checked for validation.
     - returns: A boolean value. True if validation is successful; False if validation fails.
     */
    public override func validate(_ value: String) -> Bool {
        return value.count == length
    }
}
