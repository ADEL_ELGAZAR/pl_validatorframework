import Foundation

/**
 `PaymentCardRule` is a subclass of `Rule` that defines how an payment card is validated.
 */
class PaymentCardRule: Rule {
    /// Default types of cards to be checked number with.
    private var acceptedTypes: [PaymentCardType]
    /// Default error message to be displayed if validation fails.
    private var message : String = Localization.PaymentCard
    
    /**
     Initializes an `PaymentCardRule` object to validate an url field.
     
     - parameter acceptedTypes: Array of enum values of payment card types.
     - parameter message: String of error message.
     - returns: An initialized object, or nil if an object could not be created for some reason that would not result in an exception.
     */
    public init(acceptedTypes: [PaymentCardType], message : String){
        self.acceptedTypes = acceptedTypes.isEmpty ? PaymentCardType.allCases : acceptedTypes
        self.message = message.isEmpty ? Localization.PaymentCard : message
    }
    
    /**
     Validates a field.
     
     - parameter value: String to check for validation.
     - returns: Boolean value. True if validation is successful; False if validation fails.
     */
    func validate(_ value: String) -> Bool {
        if value.isEmpty {
            
            return false
        } else {
            let cardNumber = value
            
            guard luhnCheck(cardNumber: cardNumber) else {
                
                return false
            }
            
            guard let cardType = PaymentCardType(cardNumber: cardNumber) else {
                
                return false
            }
            
            return acceptedTypes.contains(cardType)
        }
    }
    
    /**
     check card number.
     
     - parameter cardNumber: String to check for validation.
     - returns: Boolean value. True if card number is successful.
     */
    private func luhnCheck(cardNumber: String) -> Bool {

        var sum = 0

        let reversedCharacters = cardNumber.reversed().map { String($0) }

        for (idx, element) in reversedCharacters.enumerated() {

            guard let digit = Int(element) else { return false }
            switch ((idx % 2 == 1), digit) {
            case (true, 9): sum += 9
            case (true, 0...8): sum += (digit * 2) % 9
            default: sum += digit
            }
        }
        
        return sum % 10 == 0
    }
    
    /**
     Displays an error message if a field fails validation.
     
     - returns: String of error message.
     */
    public func errorMessage() -> String {
        return message
    }
}
