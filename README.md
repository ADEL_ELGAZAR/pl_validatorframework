# PL-ValidatorFramework

A framework used to validate all fields in your view based on roles related to each field.

# Usage:
Show Sample Project

        // Other rulles provided by framework
        // note: message is optional (can be empty)

        .AlphaNumeric(msg: "")
        .Alpha(msg: "")
        .PaymentCard(acceptedTypes: [], msg: "")
        .CardExpiryMonth(msg: "")
        .CardExpiryYear(msg: "")
        .CharacterSet(set: CharacterSet, msg: "")
        .Confirmation(field: ValidatableField, msg: "")
        .Email(msg: "")
        .Float(msg: "")
        .FullName(msg: "")
        .HexColor(msg: "")
        .IPV4(msg: "")
        .ISBN(msg: "")
        .RangeLength(min: Int.min, max: Int.max, msg: "")
        .ExactLength(length: 10, msg: "")
        .MaxLength(length: Int.min, msg: "")
        .MinLength(length: Int.max, msg: "")
        .Password(msg: "")
        .PhoneNumber(msg: "")
        .Required(msg: "")
        .ZipCode(msg: "")
        .URL(msg: "")
        .Regex(regex: "", msg: "")
        .Regex(regex: .UpperCase, msg: "")
        .Regex(regex: .LowerCase, msg: "")
        .Regex(regex: .NumberCase, msg: "")

# Steps
1. create Validator Container

    	// create container that should filled with (fields with their rules)
        let valid = PL_Validator()
	
2. register array of rules to each field

        // register each field with array of rules that sould match
        // fill validator container with all registered relations
        // note: message is optional (can be empty) due to default message in framework.
        valid.registerField(txtTitle, rules: [.Required(msg: "Field is required"), .FullName()])
        valid.registerField(txtEmail, rules: [.Required(), .Email(msg: "Must contain email format")])

3. implement ValidationDelegate for callbacks

		extension ViewController: ValidationDelegate {
            // all fields are filled with valid data.
            func validationSuccessful() {
                print("success")
            }
            
            // container has fields that not have valid input data format.
            func validationFailed(_ errors: [(Validatable, ValidationError)]) {
                for error in errors {
                    print("\(error.1.errorMessage)")
                }
            }
        }

4. validate the container instance

        // start validating the container and register its call back to this view controller
        valid.validate(self)
		
5. (Optional) to implement localization in validation messages, you need to copy Localizable.strings file to your directory